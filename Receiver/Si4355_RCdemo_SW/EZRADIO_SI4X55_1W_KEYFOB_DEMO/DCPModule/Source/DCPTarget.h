/**
 *  @file DCPTarget.h
 *  @brief This header file contains the target specific includes and
 *          declarations for the whole module.
 *
 *  @author Sz. Papp
 *
 *  @date 02/22/2012
 *
 */

/*
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef DCPTARGET_H_
#define DCPTARGET_H_


/* Defines and includes environmental variables, headers, macros
 * for a given Target aimed to compile the DCP module             */

/*! stdio.h include from standard C library */
#include <stdio.h>

/*! string.h include from standard C library */
#include <string.h>

/*! DCP module configuration */
#include "DCPConfig.h"

#include "../../compiler_defs.h"
#include "../../C8051F930_defs.h"
#include "../../hardware_defs.h"

/*! Hardware communication interface functions */
#include "../../CommIF.h"

/*! Software Timer functions */
#include "../../SoftTimer.h"

#endif /* DCPTARGET_H_ */
