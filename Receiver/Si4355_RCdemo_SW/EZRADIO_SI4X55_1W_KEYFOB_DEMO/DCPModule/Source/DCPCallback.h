/**
 *  @file DCPCallback.h
 *  @brief This header file contains the core declarations for the enabled
 *          callback functions.
 *
 *  @author Sz. Papp
 *
 *  @date 02/27/2012
 *
 */

/*
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef DCPCALLBACK_H_
#define DCPCALLBACK_H_

/*****************************************************************************
 *  Global Macros & Definitions
 *****************************************************************************/

/*****************************************************************************
 *  Global Function Definitions
 *****************************************************************************/
#ifdef DCP_PACKET_RECEIVED_CALLBACK_ENABLE
void DCP_PcktRcvd_Callback(void);
#endif

#ifdef DCP_PACKET_RECEIVED_EDC_ERROR_CALLBACK_ENABLE
void DCP_PcktRcvdEDCError_Callback(void);
#endif

#ifdef DCP_UNEXPECTED_PACKET_RECEIVED_CALLBACK_ENABLE
void DCP_UnxpctdPcktRcvd_Callback(void);
#endif

#ifdef DCP_COMMAND_SET_NOT_SUPPORTED_CALLBACK_ENABLE
void DCP_CmdSetNotSupported_Callback(void);
#endif

#ifdef DCP_PACKET_SENT_CALLBACK_ENABLE
void DCP_PacketSent_Callback(void);
#endif

#ifdef DCP_LEGACY_INFO_SENT_CALLBACK_ENABLE
void DCP_LegacyInfoSent_Callback(void);
#endif

#ifdef DCP_INITIATE_FINISHED_CALLBACK_ENABLE
void DCP_InitiateFinished_Callback(void);
#endif

#ifdef DCP_INDICATION_TIMEOUT_CALLBACK_ENABLE
void DCP_IndicationTimeoutCallback(void);
#endif

#ifdef DCP_NOTACK_RECEIVED_CALLBACK_ENABLE
void DCP_NotAckReceived_Callback(void);
#endif

#ifdef DCP_ACK_TIMEOUT_CALLBACK_ENABLE
void DCP_AckTimeout_Callback(void);
#endif

#endif /* DCPCALLBACK_H_ */
