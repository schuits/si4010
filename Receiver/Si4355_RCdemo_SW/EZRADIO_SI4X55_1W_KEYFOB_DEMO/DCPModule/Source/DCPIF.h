/**
 *  @file DCPIF.h
 *  @brief This header file contains the public declarations for the
 *          communication interface and DCP Rx&Tx buffer.
 *
 *  @author Sz. Papp
 *
 *  @date 02/22/2012
 *
 */

/*
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef DCPIF_H_
#define DCPIF_H_


/*****************************************************************************
 *  Global Macros & Defines
 *****************************************************************************/

/*****************************************************************************
 *  Global Enums & Typedefs
 *****************************************************************************/

/*****************************************************************************
 *  Global Variable Definitions
 *****************************************************************************/

/*! Global DCP Rx buffer declaration */
extern SEGMENT_VARIABLE(DCPRXBuffer[DCP_RX_BUFFER_SIZE], U8, SEG_XDATA);
/*! Global DCP Tx buffer declaration */
extern SEGMENT_VARIABLE(DCPTXBuffer[DCP_TX_BUFFER_SIZE], U8, SEG_XDATA);

/*****************************************************************************
 *  Global Function Definitions
 *****************************************************************************/
/* Interface Related */
U8 DCP_IF_SendByte(U8);
U8 DCP_IF_RecvByte(U8 *);

U8 DCP_IF_SendPacket(U8 * p);
U8 DCP_IF_RecvPacket(U8 state);

U8 DCP_IF_CalcEDC(U8 * p);

/* Hardware related */
void DCP_HW_StartTimer_10ms(U8 t);
void DCP_HW_StopTimer(void);
U8  DCP_HW_TimerElapsed(void);


#endif /* DCPIF_H_ */
