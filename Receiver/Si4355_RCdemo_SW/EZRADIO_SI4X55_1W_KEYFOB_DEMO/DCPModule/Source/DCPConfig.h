/**
 *  @file DCPConfig.h
 *  @brief The configuration of the DCP Module can be done in this header file.
 *
 *  @author Sz. Papp
 *
 *  @date 02/22/2012
 *
 */

/*
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef DCPCONFIG_H_
#define DCPCONFIG_H_

/*****************************************************************************
 *  Global Macros & Definitions
 *****************************************************************************/

/* Defines related to DCP communication
 *****************************************************************************/

/*! Timeout in case of ACK / NotACK message ( x 10ms ) */
#define DCP_MSG_ACKNOWLEDGE_TIMEOUT     15u

/*! Timeout in case of indication message ( x 10ms ) */
#define DCP_MSG_INDICATION_TIMEOUT      40u

/*! DCP receive buffer size */
#define DCP_RX_BUFFER_SIZE              128u

/*! DCP transmit buffer size */
#define DCP_TX_BUFFER_SIZE              128u


/* !Callback Functions - Uncomment to enable
 *****************************************************************************/

/* #define DCP_PACKET_RECEIVED_CALLBACK_ENABLE            */

/* #define DCP_PACKET_RECEIVED_EDC_ERROR_CALLBACK_ENABLE  */

/* #define DCP_UNEXPECTED_PACKET_RECEIVED_CALLBACK_ENABLE */

/* #define DCP_COMMAND_SET_NOT_SUPPORTED_CALLBACK_ENABLE  */

/* #define DCP_PACKET_SENT_CALLBACK_ENABLE                */

/* #define DCP_LEGACY_INFO_SENT_CALLBACK_ENABLE           */

/* #define DCP_INITIATE_FINISHED_CALLBACK_ENABLE          */

/* #define DCP_INDICATION_TIMEOUT_CALLBACK_ENABLE         */

/* #define DCP_NOTACK_RECEIVED_CALLBACK_ENABLE            */

/* #define DCP_ACK_TIMEOUT_CALLBACK_ENABLE                */


/*! DCP Communication interface selection - Uncomment to enable
 *****************************************************************************/
#define DCP_IF_UART
/*  #define DCP_IF_SPI  */
/*  #define DCP_IF_USB  */
/*  #define DCP_IF_I2C  */


#endif /* DCPCONFIG_H_ */
