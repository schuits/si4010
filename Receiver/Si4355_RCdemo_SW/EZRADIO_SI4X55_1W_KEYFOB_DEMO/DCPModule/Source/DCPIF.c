/**
 *  @file DCPIF.c
 *  @brief This source file contains the definitions of the interface
 *          functions for the DCP communication peripheral defined in
 *          DCPConfig.h and functions for Timer peripheral as well.
 *
 *  @author Sz. Papp
 *
 *  @date 02/22/2012
 *
 */

/*
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


/*****************************************************************************
 *  Includes
 *****************************************************************************/

/*! Target include collection */
#include "DCPTarget.h"

/*! DCP Core header */
#include "DCPCore.h"

/*! DCP Interface functions */
#include "DCPIF.h"

/*! DCP Callback functions */
#include "DCPCallback.h"

/*****************************************************************************
 *  Local Functions
 *****************************************************************************/
//U8 DCP_IF_SendByte(U8);
//U8 DCP_IF_RecvByte(U8 *);
U8 DCP_IF_SendTextInfo(tAsciiInfo *);

#ifdef DCP_IF_UART
  /* Function protos */
  U8 DCP_IF_SendUART(U8 );
  U8 DCP_IF_RecvUART(U8 *);
#endif
#ifdef DCP_IF_SPI
  /* Function protos */
  U8 DCP_IF_SendSPI(U8 );
  U8 DCP_IF_RecvSPI(U8 *);
#endif
#ifdef DCP_IF_I2C
  /* Function protos */
  U8 DCP_IF_SendI2C(U8 );
  U8 DCP_IF_RecvI2C(U8 *);
#endif
#ifdef DCP_IF_USB
  /* Function protos */
  U8 DCP_IF_SendUSB(U8 );
  U8 DCP_IF_RecvUSB(U8 *);
#endif


/*****************************************************************************
 *  Global Variables
 *****************************************************************************/

/*! Global DCP Rx buffer definition */
SEGMENT_VARIABLE(DCPRXBuffer[DCP_RX_BUFFER_SIZE], U8, SEG_XDATA);
/*! Global DCP Tx buffer definition */
SEGMENT_VARIABLE(DCPTXBuffer[DCP_TX_BUFFER_SIZE], U8, SEG_XDATA);

/**
 *  Sends a DCP2 packet on the configured interface.
 *
 *  @param[in]  *p Pointer to DCP packet
 *
 *  @return     Return the result of transmitting.
 *
 *  @note       Must be called periodically until returns other than FALSE.
 *
 *  @author     Sz. Papp
 *
 *****************************************************************************/
U8 DCP_IF_SendPacket(U8 * p)
{
  U8 len;

  /* Get Packet Length */
  len = *(p + 1u) + 2u;

  while(len)
  {
    if (DCP_IF_SendByte(*p) == TRUE)
    {
      len -= 1u;
      p += 1u;
    }
  }

  /* Callback Function invocation */
  #ifdef DCP_PACKET_SENT_CALLBACK_ENABLE
  DCP_PacketSent_Callback();
  #endif

  return TRUE;
}

/**
 *  Receives a DCP2 packet on the configured interface.
 *
 *  @param[in]  state <br> \b 0u - Normal State <br> \b 1u - Reset SM
 *
 *  @return     FALSE - No packet received / TRUE  - New packet received
 *
 *  @note       Called inside DCP_Pollhandler. Not necessary to invoke by
 *              user application.
 *
 *  @author     Sz. Papp
 *
 *****************************************************************************/
U8 DCP_IF_RecvPacket(U8 state)
{

  U8 retVal = 0u;
  U8 edc;

  static U8 len;
  static U8 pos;
  static lDCPRecvState = DCP_SM_START;

  if (0u == state)
  {
    /* Normal mode */

    switch (lDCPRecvState)
    {

    case DCP_SM_START:
      /* Check for Start-Of-Frame char */
      if (DCP_IF_RecvByte(&DCPRXBuffer[0u]) == TRUE)
      {
        if (0x55 == DCPRXBuffer[0u])
        {
          /* SOF received */
          lDCPRecvState = DCP_SM_STEP1;
        }

        /* Legacy support for Identification */
        if ('I' == (DCPRXBuffer[0u] & 0xDF))
        {
          /* Fill the Buffer with ASCII info string */

          /* Send packet back */
          DCP_IF_SendTextInfo(&Ascii_Info);
        }
      }

      break;

    case DCP_SM_STEP1:
      /* Get length of the packet */
      if (DCP_IF_RecvByte(&DCPRXBuffer[1u]) == TRUE)
      {
        /* LEN received */
        len = DCPRXBuffer[1u];
        pos = 2u;

        lDCPRecvState = DCP_SM_STEP2;
      }

      break;

    case DCP_SM_STEP2:
      /* Receive packet */
      if (len > 0u)
      {
        if (DCP_IF_RecvByte(&DCPRXBuffer[pos]) == TRUE)
        {
          len -= 1u;
          pos += 1u;
        }
      }
      else
      {
        /* Packet in buffer */

        lDCPRecvState = DCP_SM_STEP3;
      }

      break;

    case DCP_SM_STEP3:
      /* Check EDC */
      edc = 0u;

      for (len = 1u; len < pos; len++)
      {
        edc ^= DCPRXBuffer[len];
      }

      if (edc == 0u)
      {
        /* EDC OK */
        DCPInternalData.Flags &= ~(1u << DCP_RX_WRONGEDC);

        /* Callback Function invocation */
        #ifdef DCP_PACKET_RECEIVED_CALLBACK_ENABLE
        DCP_PcktRcvd_Callback();
        #endif
      }
      else
      {
        /* Wrong EDC */
        DCPInternalData.Flags |= (1u << DCP_RX_WRONGEDC);

        /* Callback Function invocation */
        #ifdef DCP_PACKET_RECEIVED_EDC_ERROR_CALLBACK_ENABLE
        DCP_PcktRcvdEDCError_Callback();
        #endif
      }

      DCP_IF_RecvPacket(1u);
      retVal = 1u;
      break;

    default:
      /* Not allowed state -> Reset */
      DCP_IF_RecvPacket(1u);
      break;
    }
  }
  else
  {
    /* Reset mode */

    lDCPRecvState = DCP_SM_START;
    len = 0u;
    pos = 0u;
  }

  return retVal;
}

/**
 *  Sends a byte on the configured interface.
 *  May be buffered on the interface driver side.
 *
 *  @param[in]  byte  Byte to send
 *
 *  @return     Result of transferring <br>
 *                    \b FALSE - Failed <br>
 *                    \b TRUE  - Success
 *
 *  @note       DCP_IF_Send |UART/SPI/I2C/USB| according to the predefined
 *              DCP communication interface.
 *
 *  @author     Sz. Papp
 *
 *****************************************************************************/
#ifdef DCP_IF_UART
U8 DCP_IF_SendUART(U8 byte)
{
  return Comm_IF_SendUART(byte);
}
#endif

#ifdef DCP_IF_SPI
U8 DCP_IF_SendSPI(U8 byte)
{

}
#endif

#ifdef DCP_IF_I2C
U8 DCP_IF_SendI2C(U8 byte)
{

}
#endif

#ifdef DCP_IF_USB
U8  DCP_IF_SendUSB(U8 byte)
{

}
#endif

/**
 *  Receives a byte on the configured interface. May be buffered
 *  on the interface driver side.
 *
 *  @param[out] *byte Received byte
 *
 *  @return     Result of receiving: <br>
 *                \b FALSE - no new byte <br>
 *                \b TRUE  - new byte received
 *
 *  @note       DCP_IF_Recv |UART/SPI/I2C/USB| according to the predefined
 *              DCP communication interface.
 *
 *  @author     Sz. Papp
 *
 *****************************************************************************/
#ifdef DCP_IF_UART
U8 DCP_IF_RecvUART(U8 * byte)
{
  return Comm_IF_RecvUART(byte);
}
#endif

#ifdef DCP_IF_SPI
U8  DCP_IF_RecvSPI(U8 * byte)
{

}
#endif

#ifdef DCP_IF_I2C
U8  DCP_IF_RecvI2C(U8 * byte)
{

}
#endif

#ifdef DCP_IF_USB
U8  DCP_IF_RecvUSB(U8 * byte)
{

}
#endif

/**
 *  Sent one byte on the predefined interface.
 *  (workaround for 8051 (Keil) inefficient function pointer handling)
 *
 *  @param[in]  byte Byte to be sent
 *
 *  @return     Result of transmitting: <br>
 *                \b FALSE - Failed <br>
 *                \b TRUE  - Success
 *
 *  @note
 *
 *  @author     Sz. Papp
 *
 *****************************************************************************/
U8 DCP_IF_SendByte(U8 byte)
{
#ifdef DCP_IF_UART
  return DCP_IF_SendUART(byte);
#endif
#ifdef DCP_IF_SPI
  return DCP_IF_SendSPI(byte);
#endif
#ifdef DCP_IF_I2C
  return DCP_IF_SendI2C(byte);
#endif
#ifdef DCP_IF_USB
  return DCP_IF_SendUSB(byte);
#endif
}

/**
 *  Check for a received data.
 *  (workaround for 8051 (Keil) inefficient function pointer handling)
 *
 *  @param[out] *byte Pointer into write the received byte buffer
 *
 *  @return     Result of receiving: <br>
 *                \b FALSE - No new byte received <br>
 *                \b TRUE  - New byte received
 *
 *  @note
 *
 *  @author     Sz. Papp
 *
 *****************************************************************************/
U8 DCP_IF_RecvByte(U8 * byte)
{
#ifdef DCP_IF_UART
  return DCP_IF_RecvUART(byte);
#endif
#ifdef DCP_IF_SPI
  return DCP_IF_RecvSPI(byte);
#endif
#ifdef DCP_IF_I2C
  return DCP_IF_RecvI2C(byte);
#endif
#ifdef DCP_IF_USB
  return DCP_IF_RecvUSB(byte);
#endif
}

/**
 *  Calculate the EDC for a given packet.
 *
 *  @param[in]  *p  Pointer to DCP packet.
 *
 *  @return     Calculated EDC.
 *
 *  @note
 *
 *  @author     Sz. Papp
 *
 *****************************************************************************/
U8 DCP_IF_CalcEDC(U8 * p)
{
  U8 ii;
  U8 len;
  U8 lEDC = 0u;

  /* Skip SOF */
  p++;

  /* Get DCP packet length */
  len = *p;

  for (ii = 0u; ii < len; ii++)
  {
    lEDC ^= *p;
    p++;
  }

  return lEDC;
}

/**
 *  Sends a legacy identification string on the predefined interface.
 *
 *  @param[in]  *pAsciiInfo Pointer to legacy Text Info structure.
 *
 *  @return     Return the result of transmitting.
 *
 *  @note
 *
 *  @author     Sz. Papp
 *
 *****************************************************************************/
U8 DCP_IF_SendTextInfo(tAsciiInfo *pAsciiInfo)
{
  U8 *p;

  /* Send HW_NAME field */
  p = (U8 *) pAsciiInfo->HW_NAME;
  while(*p)
    if (DCP_IF_SendByte(*p) == TRUE)
      p += 1u;
  while (DCP_IF_SendByte(',') == FALSE);

  /* Send HW_VERSION field */
  p = (U8 *) pAsciiInfo->HW_VERSION;
  while(*p)
    if (DCP_IF_SendByte(*p) == TRUE)
      p += 1u;
  while (DCP_IF_SendByte(',') == FALSE);

  /* Send APP_FW_VERSION field */
  p = (U8 *) pAsciiInfo->APP_FW_VERSION;
  while(*p)
    if (DCP_IF_SendByte(*p) == TRUE)
      p += 1u;
  while (DCP_IF_SendByte(',') == FALSE);

  /* Send APP_FW_NAME field */
  p = (U8 *) pAsciiInfo->APP_FW_NAME;
  while(*p)
    if (DCP_IF_SendByte(*p) == TRUE)
      p += 1u;

  /* Callback Function invocation */
  #ifdef DCP_LEGACY_INFO_SENT_CALLBACK_ENABLE
  DCP_LegacyInfoSent_Callback();
  #endif

  return TRUE;
}

/**
 *  Start the Timer peripheral.
 *
 *  @param[in]  t Time amount to wait for in 10ms steps.
 *
 *  @return
 *
 *  @note
 *
 *  @author     Sz. Papp
 *
 *****************************************************************************/
void DCP_HW_StartTimer_10ms(U8 t)
{
  SoftTimer_Add(t, SOFTTIMER_CH0);
}

/**
 *  Stops the Timer peripheral.
 *
 *  @return
 *
 *  @note
 *
 *  @author Sz. Papp
 *
 *****************************************************************************/
void DCP_HW_StopTimer(void)
{
  SoftTimer_Remove(SOFTTIMER_CH0);
}

/**
 *  Checks if the Time elapsed or not.
 *
 *  @return   Timer state <br>
 *            \b TRUE:  Time elapsed <br>
 *            \b FALSE: Not elapsed yet
 *
 *  @note
 *
 *  @author   Sz. Papp
 *
 *****************************************************************************/
U8 DCP_HW_TimerElapsed(void)
{
  return SoftTimer_Elapsed(SOFTTIMER_CH0);
}

