/**
 *  @file DCPCore.h
 *  @brief This header file contains the function and variable declarations for
 *          the DCP2 Software Module core functionality, as it contains the
 *          related type definitions, enumerations and macros as well.
 *
 *  @author Sz. Papp
 *
 *  @date 02/22/2012
 *
 */

/*
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#ifndef DCPCORE_H_
#define DCPCORE_H_

/*****************************************************************************
 *  Global Enums & Typedefs
 *****************************************************************************/

/*! Enum contains the SM states */
typedef enum
{
  DCP_SM_START,
  DCP_SM_STEP1,
  DCP_SM_STEP2,
  DCP_SM_STEP3,
  DCP_SM_STEP4,
  DCP_SM_STEP5,
  DCP_SM_STEP6,
  DCP_SM_STEP7,
  DCP_SM_STEP8,
  DCP_SM_STEP9,
  DCP_SM_STEP10,
  DCP_SM_STEP11,
  DCP_SM_STEP12,
  DCP_SM_STEP13,
  DCP_SM_STEP14
} eDCPStateMachineStates;

/*! Enum contains the LED states */
typedef enum
{
  DCP_RX_WRONGEDC,
  DCP_TX_INITIATED
} eDCPFlags;

/*! Enum contains the DCP receiver SM states */
typedef enum
{
  DCP_RXIDLE,
  DCP_RXREQ,
  DCR_RXREQACK,
  DCP_RXIND,
  DCP_RXINDACK
} eDCPRXStates;

/*! Enum contains the DCP transmitter SM states */
typedef enum
{
  DCP_TXIDLE,
  DCP_TXREQ,
  DCR_TXREQACK,
  DCP_TXIND,
  DCP_TXINDACK
} eDCPTXStates;

/*! Enum contains the possible NotACK causes */
typedef enum
{
  NOTACK_EDC_ERROR,
  NOTACK_COMMAND_SET_NOT_SUPPORTED,
  NOTACK_OUT_OF_SYNC,
  NOTACK_UNEXPECTED
} eNotACKs;

/*! Enum contains the DCP Message types states */
typedef enum
{
  MSG_REQUEST,
  MSG_ACKNOWLEDGE,
  MSG_NOTACKNOWLEDGE,
  MSG_INDICATION
} eDCPMessageTypes;

/*! Enum contains the possible return values for Initiate */
typedef enum
{
  DCP_RES_BUSY,
  DCP_RES_OK,
  DCP_RES_TIMEOUT_ACK,
  DCP_RES_TIMEOUT_IND,
  DCP_RES_NOTACK_ERROR,
  DCP_RES_EDC_ERROR,
  DCP_RES_UNEXPECTED_PACKET_ERROR
} eDCPInitiateResults;

/*! Struct type that holds the internal variables of DCP Module */
typedef struct
{
  U8  Flags;
  U8  RXState;
  U8  TXState;
  U8  TXCommandSet;
  U8  TXLength;
  U8  SyncCounter;
  U8  CommandSetIndex;
  U8  DCPCoreState;
  U8 *pTxData;
} tDCPInternalData;

/*****************************************************************************
 *  Global Macros & Definitions
 *****************************************************************************/

/*! Macro that extracts the Sync counter from a DCP packet */
#define DCP_GETSYNC(pRxPacket)  (*((pRxPacket) + 2u) & 0x03);

/*****************************************************************************
 *  Global Variable Definitions
 *****************************************************************************/

/*! Global variable for DCP internal variables */
extern tDCPInternalData DCPInternalData;

/*****************************************************************************
 *  Global Function Definitions
 *****************************************************************************/
void DCP_Init(void);
void DCP_Pollhandler(void);
eDCPInitiateResults DCP_Initiate(U8, U8, U8 *);


#endif /* DCPCORE_H_ */
