/**
 *  @file DCPCommand.c
 *  @brief This source file contains the implementation of the supported
 *          DCP Command Set handler functions.
 *
 *  @author Sz. Papp
 *
 *  @date 02/22/2012
 *
 */

/*
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*****************************************************************************
 *  Includes
 *****************************************************************************/
#include "DCPTarget.h"
#include "DCPCore.h"
#include "DCPCommand.h"
#include "DCPIF.h"

/*****************************************************************************
 *  Local Function Definitions
 *****************************************************************************/
U8 DCP_GeneralCommandSetHandler(U8 *);

/*****************************************************************************
 *  Global Variable Definitions
 *****************************************************************************/

/*! Supported DCP2 Command Sets definition */
const SEGMENT_VARIABLE(DCP_SupportedCommandSets[], tCommandSets, SEG_CODE) = {
    { COMMAND_SET_GENERAL , DCP_GeneralCommandSetHandler }
  };

/*****************************************************************************
 *  Local Macros & Defines
 *****************************************************************************/

/*! This macro gives the count of supported DCP Command Sets */
#define NUMOF_CMDSET  (sizeof(DCP_SupportedCommandSets)/sizeof(tCommandSets))

/**
 *  Checks if the command set in DCP2 packet pointed by pRxPacket is supported
 *  or not.
 *
 *  @param[in]  *pRxPacket Pointer to RXBuffer holding the DCP packet.
 *
 *  @return     Supported/Not Supported (TRUE/FALSE)
 *
 *  @author     Sz. Papp
 *
 *****************************************************************************/
U8 DCP_CommandSetSupported(U8 * pRxPacket)
{
  U8 ii;

  for (ii = 0u; ii < NUMOF_CMDSET; ii++)
  {
    if (DCP_SupportedCommandSets[ii].CmdSet == ((*(pRxPacket + 2u) & 0x3C) >> 2u))
    {
      /* Save the array index of the command set */
      DCPInternalData.CommandSetIndex = ii;

      return TRUE;
    }
  }

  return FALSE;
}


/**
 *  General Command Set Handler
 *
 *  @param[in,out] *pRxPacket - Pointer to RXBuffer
 *
 *  @return         Done / Processing (TRUE/FALSE)
 *
 *  @note           The function gets the received DCP Request packet thru its
 *                  pointer parameter and must write its Indication response
 *                  in the same buffer also.
 *
 *  @note           Must be called periodically until returns TRUE.
 *
 *  @author         Sz. Papp
 *
 *****************************************************************************/
U8 DCP_GeneralCommandSetHandler(U8 * pRxPacket)
{
  U8 ii = 0;
  tGeneralCommandSet *pGeneralCS = (tGeneralCommandSet *) pRxPacket;

  /* Process the data according to the subCommand */
  switch (pGeneralCS->subCommand)
  {

  case GENERAL_CS_GET_DEVICE_INFO_REQ:
    /* Device Info Indication packet compilation */

    pGeneralCS->Length = 5u + strlen(Ascii_Info.HW_NAME) + strlen(Ascii_Info.HW_VERSION) +
                         strlen(Ascii_Info.APP_FW_VERSION) + strlen(Ascii_Info.APP_FW_NAME);

    /* Set Control byte in answer */
    pGeneralCS->Control = (MSG_INDICATION << 6u) | (COMMAND_SET_GENERAL << 2u) | DCPInternalData.SyncCounter;

    /* Set subCommand to device info indication */
    pGeneralCS->subCommand = GENERAL_CS_DEVICE_INFO_IND;

    /* Set pointer to the beginning of data field */
    pRxPacket = ((U8 *) &pGeneralCS->subCommand) + 1u;

    /* Copy HW_NAME */
    memcpy(pRxPacket, Ascii_Info.HW_NAME, strlen(Ascii_Info.HW_NAME));
    pRxPacket += strlen(Ascii_Info.HW_NAME);

    /* Copy HW_VERSION */
    memcpy(pRxPacket, Ascii_Info.HW_VERSION, strlen(Ascii_Info.HW_VERSION));
    pRxPacket += strlen(Ascii_Info.HW_VERSION);

    /* Copy APP_FW_VERSION */
    memcpy(pRxPacket, Ascii_Info.APP_FW_VERSION, strlen(Ascii_Info.APP_FW_VERSION));
    pRxPacket += strlen(Ascii_Info.APP_FW_VERSION);

    /* Copy APP_FW_NAME */
    memcpy(pRxPacket, Ascii_Info.APP_FW_NAME, strlen(Ascii_Info.APP_FW_NAME));
    pRxPacket += strlen(Ascii_Info.APP_FW_NAME);

    /* Supported Command Sets list */
    *pRxPacket++ = COMMAND_SET_GENERAL;
    *pRxPacket++ = COMMAND_SET_LAB_MEASURE;

    /* Calculate EDC */
    *pRxPacket = DCP_IF_CalcEDC((U8 *) pGeneralCS);

    break;

  default:
    /* Command not supported */

    /* DCP Indication packet compilation */
    pGeneralCS->Length = 4u;

    /* Set Control byte in answer */
    pGeneralCS->Control = (MSG_INDICATION << 6u) | (COMMAND_SET_GENERAL << 2u) | DCPInternalData.SyncCounter;

    /* Set subCommand to device info indication */
    pGeneralCS->subCommand = GENERAL_CS_INDICATION;

    pRxPacket = ((U8 *) &pGeneralCS->subCommand) + 1u;

    *pRxPacket++ = GENERAL_CS_COMMAND_NOT_SUPPORTED;

    *pRxPacket = DCP_IF_CalcEDC((U8 *) pGeneralCS);

    break;
  }

  return TRUE;
}
