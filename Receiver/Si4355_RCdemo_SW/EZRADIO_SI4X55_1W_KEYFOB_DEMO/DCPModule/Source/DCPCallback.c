/**
 *  @file DCPCallback.c
 *  @brief This source file contains the core implementation of the DCP
 *          Software Module.
 *
 *  @author Sz. Papp
 *
 *  @date 02/22/2012
 *
 */

/*
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*****************************************************************************
 *  Includes
 *****************************************************************************/
#include "DCPTarget.h"
#include "DCPCallback.h"

/*****************************************************************************
 *  Global Functions
 *****************************************************************************/

#ifdef DCP_PACKET_RECEIVED_CALLBACK_ENABLE
/**
 *  Callback function for Packet Received Event.
 *
 *  @param
 *
 *  @return
 *
 *  @author
 *
 *****************************************************************************/
void DCP_PcktRcvd_Callback(void)
{

}
#endif

#ifdef DCP_PACKET_RECEIVED_EDC_ERROR_CALLBACK_ENABLE
/**
 *  Callback function for Packet Received with EDC Error Event.
 *
 *  @param
 *
 *  @return
 *
 *  @author
 *
 *****************************************************************************/
void DCP_PcktRcvdEDCError_Callback(void)
{

}
#endif

#ifdef DCP_UNEXPECTED_PACKET_RECEIVED_CALLBACK_ENABLE
/**
 *  Callback function for Unexpected Packet Received Event.
 *
 *  @param
 *
 *  @return
 *
 *  @author
 *
 *****************************************************************************/
void DCP_UnxpctdPcktRcvd_Callback(void)
{

}
#endif

#ifdef DCP_COMMAND_SET_NOT_SUPPORTED_CALLBACK_ENABLE
/**
 *  Callback function for Command Set not supported Event.
 *
 *  @param
 *
 *  @return
 *
 *  @author
 *
 *****************************************************************************/
void DCP_CmdSetNotSupported_Callback(void)
{

}
#endif

#ifdef DCP_PACKET_SENT_CALLBACK_ENABLE
/**
 *  Callback function for Packet Sent Event.
 *
 *  @param
 *
 *  @return
 *
 *  @author
 *
 *  Description:
 *
 *****************************************************************************/
void DCP_PacketSent_Callback(void)
{

}
#endif

#ifdef DCP_LEGACY_INFO_SENT_CALLBACK_ENABLE
/**
 *  Callback function for Legacy Info sent Event.
 *
 *  @param
 *
 *  @return
 *
 *  @author
 *
 *****************************************************************************/
void DCP_LegacyInfoSent_Callback()
{

}
#endif

#ifdef DCP_INITIATE_FINISHED_CALLBACK_ENABLE
/**
 *  Callback function if initiated DCP session finishes.
 *
 *  @param
 *
 *  @return
 *
 *  @author
 *
 *****************************************************************************/
void DCP_InitiateFinished_Callback()
{

}
#endif

#ifdef DCP_INDICATION_TIMEOUT_CALLBACK_ENABLE
/**
 *  Callback function if Indication message not received.
 *
 *  @param
 *
 *  @return
 *
 *  @author
 *
 *****************************************************************************/
void DCP_IndicationTimeoutCallback()
{

}
#endif

#ifdef DCP_NOTACK_RECEIVED_CALLBACK_ENABLE
/**
 *  Callback function if NotACK packet received.
 *
 *  @param
 *
 *  @return
 *
 *  @author
 *
 *****************************************************************************/
void DCP_NotAckReceived_Callback()
{

}
#endif

#ifdef DCP_ACK_TIMEOUT_CALLBACK_ENABLE
/**
 *  Callback function if no ACK packet received.
 *
 *  @param
 *
 *  @return
 *
 *  @author
 *
 *****************************************************************************/
void DCP_AckTimeout_Callback()
{

}
#endif

