/**
 *  @file DCPCore.c
 *  @brief This source file contains the core implementation of the DCP Software
 *          Module.
 *
 *  @author Sz. Papp
 *
 *  @date 02/22/2012
 *
 *
 *  @mainpage DCP2 Software Module Documentation
 *
 *  @par Overview
 *  This documentation is ... TBD!
 *
 *  @par Block diagram
 *  Block diagram ... TBD.
 *
 */

/*
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* FUTURE IMPROVEMENTS (TODO):
 * DCP2: Objektum-orient�lt m�don, absztrakt oszt�ly -> typedef struct
 * p�ld�nyos�t -> n elem� t�mb l�trehoz�sa. Egyes p�ld�nyok k�l�nb�z�
 * interf�szekkel.K�lcs�n�s kiz�r�s, ha dead-lock alakulna ki, pl.
 * DCP2 gateway -> t�bb interf�szen egyszerre kezd�dik tranzakci� ami
 * bevonja a m�sik interf�szt is (protokoll szinten nem megoldott jelenleg).
 * T�bb session p�rhuzamosan egy DCP2 interf�szen. Max. 3 jelenleg a Sync
 * Counter miatt korl�t.
 *
 */


/*****************************************************************************
 *  Includes
 *****************************************************************************/
#include "DCPTarget.h"
#include "DCPIF.h"
#include "DCPCommand.h"
#include "DCPCore.h"
#include "DCPCallback.h"
#include "DCPConfig.h"

/*****************************************************************************
 *  Local Macros & Definitions
 *****************************************************************************/
#if (defined DCP_IF_UART) && ((defined DCP_IF_SPI) || (defined DCP_IF_I2C) || (defined DCP_IF_USB))
#error "Only one DCP interface must be defined!"
#endif
#if (defined DCP_IF_SPI) && ((defined DCP_IF_UART) || (defined DCP_IF_I2C) || (defined DCP_IF_USB))
#error "Only one DCP interface must be defined!"
#endif
#if (defined DCP_IF_I2C) && ((defined DCP_IF_SPI) || (defined DCP_IF_UART) || (defined DCP_IF_USB))
#error "Only one DCP interface must be defined!"
#endif
#if (defined DCP_IF_USB) && ((defined DCP_IF_SPI) || (defined DCP_IF_I2C) || (defined DCP_IF_UART))
#error "Only one DCP interface must be defined!"
#endif
#if (!defined DCP_IF_UART) && (!defined DCP_IF_SPI) && (!defined DCP_IF_I2C) && (!defined DCP_IF_USB)
#error "One DCP interface must be defined!"
#endif

/*****************************************************************************
 *  Global Variables
 *****************************************************************************/
tDCPInternalData DCPInternalData;

/*****************************************************************************
 *  Local Variables
 *****************************************************************************/

/*****************************************************************************
 *  Local Function Definitions
 *****************************************************************************/
U8 DCP_SendAck(void);
U8 DCP_SendNotAck(eNotACKs e);
eDCPMessageTypes DCP_GetMessageType(U8 * p);


/**
 *  Initialize the DCP module.
 *
 *  @author Sz. Papp
 *
 *****************************************************************************/
void DCP_Init()
{
  /* Reset state */
  DCP_IF_RecvPacket(1u);

  /* Set DCP internal state machines to Idle state */
  DCPInternalData.DCPCoreState = DCP_SM_START;
  DCPInternalData.TXState = DCP_SM_START;
  DCPInternalData.RXState = DCP_SM_START;

  /* Clear flags*/
  DCPInternalData.Flags = 0u;

  /* Reset Sync counter */
  DCPInternalData.SyncCounter = 0u;
}

/**
 *  Main poll handler for DCP module.
 *
 *  @note   Must be called periodically.
 *
 *  @author Sz. Papp
 *
 *****************************************************************************/
void DCP_Pollhandler()
{
  U8 * plTXData;

  switch (DCPInternalData.DCPCoreState)
  {

  case DCP_SM_START:
    /* DCP Idle state */

    /* Check for received DCP packet */
    if (DCP_IF_RecvPacket(0u) == TRUE)
    {
      /* Packet in DCPRXBuffer */

      /* Check EDC */
      if (DCPInternalData.Flags & (1u << DCP_RX_WRONGEDC))
      {
        /* Send NotACK with Wrong EDC */
        DCP_SendNotAck(NOTACK_EDC_ERROR);

        break;
      }

      if (DCP_GetMessageType(DCPRXBuffer) != (eDCPMessageTypes) MSG_REQUEST)
      {
        /* If the session start message not Request */
        DCP_SendNotAck(NOTACK_UNEXPECTED);

        /* Callback Function invocation */
        #ifdef DCP_UNEXPECTED_PACKET_RECEIVED_CALLBACK_ENABLE
        DCP_UnxpctdPcktRcvd_Callback();
        #endif

        break;
      }

      /* Check if Command Set is supported */
      if (DCP_CommandSetSupported(DCPRXBuffer) == FALSE)
      {
        /* Send NotACK with Command Set not supported */
        DCP_SendNotAck(NOTACK_COMMAND_SET_NOT_SUPPORTED);

        /* Callback Function invocation */
        #ifdef DCP_COMMAND_SET_NOT_SUPPORTED_CALLBACK_ENABLE
        DCP_CmdSetNotSupported_Callback();
        #endif

        break;
      }

      /* Save Sync Counter */
      DCPInternalData.SyncCounter = DCP_GETSYNC(DCPRXBuffer);

      /* Send Acknowledge */
      DCP_SendAck();

      /* Process the DCP2 packet */
      DCPInternalData.DCPCoreState = DCP_SM_STEP1;
    }
    else if (DCPInternalData.Flags & (1u << DCP_TX_INITIATED))
    {
      /* DCP transmit initiated */

      /* DCP Request packet compilation */
      DCPTXBuffer[0u] = 0x55;                                           /* Start-Of-Frame */
      DCPTXBuffer[1u] = DCPInternalData.TXLength + 2u;                  /* Length + 2 byte for EDC & CTRL byte */
      DCPTXBuffer[2u] = (MSG_REQUEST << 6u) |                           /* Control byte */
                        ((DCPInternalData.TXCommandSet & 0x0F) << 2u) |
                        (DCPInternalData.SyncCounter & 0x03);

      /* Set local pointer to DCP TX Buffer*/
      plTXData = &DCPTXBuffer[3u];

      /* Copy the data to DCP TX buffer */
      while (DCPInternalData.TXLength)
      {
        *plTXData = *DCPInternalData.pTxData;
        plTXData += 1u;
        DCPInternalData.pTxData += 1u;
        DCPInternalData.TXLength -= 1u;
      }

      /* Calculate EDC */
      *plTXData = DCP_IF_CalcEDC(DCPTXBuffer);

      /* Send the DCP Request packet */
      DCP_IF_SendPacket(DCPTXBuffer);

      /* Set ACK timeout */
      DCP_HW_StartTimer_10ms(DCP_MSG_ACKNOWLEDGE_TIMEOUT);

      DCPInternalData.DCPCoreState = DCP_SM_STEP3;
    }

    break;

  case DCP_SM_STEP1:
    /* Invoke the Command Set Handler */

    if (DCP_SupportedCommandSets[DCPInternalData.CommandSetIndex].CmdHandler(DCPRXBuffer) == TRUE)
    {
      /* Indication ready to send */
      DCP_IF_SendPacket(DCPRXBuffer);

      /* Start Timeout counter */
      DCP_HW_StartTimer_10ms(DCP_MSG_ACKNOWLEDGE_TIMEOUT);

      /* Wait for ACK */
      DCPInternalData.DCPCoreState = DCP_SM_STEP2;
    }

    break;

  case DCP_SM_STEP2:
    /* Wait for ACK to close session */

    /* Check for received DCP packet */
    if (DCP_IF_RecvPacket(0u) == TRUE)
    {
      /* Packet in DCPRXBuffer */

      if (DCP_GetMessageType(DCPRXBuffer) == (eDCPMessageTypes) MSG_ACKNOWLEDGE)
      {
        /* Session closed with success */

      }
      else if (DCP_GetMessageType(DCPRXBuffer) == (eDCPMessageTypes) MSG_NOTACKNOWLEDGE)
      {
        /* Session closed with error */

      }
      else
      {
        /* Session closed with unexpected packet */

        DCP_SendNotAck(NOTACK_UNEXPECTED);
      }

      DCPInternalData.DCPCoreState = DCP_SM_START;
    }
    /* Check if Timeout already elapsed */
    else if (DCP_HW_TimerElapsed() == TRUE)
    {
      /* Error -> no ACK received from Master */

      /* Reset state of receiver */
      DCP_IF_RecvPacket(1u);

      DCPInternalData.DCPCoreState = DCP_SM_START;
    }

    break;

  case DCP_SM_STEP3:
    /* Waiting for ACK return */

    if (DCP_IF_RecvPacket(0u) == TRUE)
    {
      /* Is EDC Ok? */
      if (DCPInternalData.Flags & (1u << DCP_RX_WRONGEDC))
      {
        /* Change the DCP_Initiate internal state machine state to RX EDC error */
        DCPInternalData.TXState = DCP_SM_STEP5;

        /* DCP Module Idle state */
        DCPInternalData.DCPCoreState = DCP_SM_START;
      }
      /* Check if ACK/NotACK */
      else if (DCP_GetMessageType(DCPRXBuffer) == (eDCPMessageTypes) MSG_ACKNOWLEDGE)
      {
        /* ACK received */

        /* Start Timeout for Indication message  */
        DCP_HW_StartTimer_10ms(DCP_MSG_INDICATION_TIMEOUT);

        DCPInternalData.DCPCoreState = DCP_SM_STEP4;
      }
      else if (DCP_GetMessageType(DCPRXBuffer) == (eDCPMessageTypes) MSG_NOTACKNOWLEDGE)
      {
        /* NotACK received */

        /* Change the DCP_Initiate internal state machine state to NotACK Received error */
        DCPInternalData.TXState = DCP_SM_STEP3;

        /* DCP Module Idle state */
        DCPInternalData.DCPCoreState = DCP_SM_START;
      }
      else
      {
        /* UNEXPECTED PACKET received */

        /* Change the DCP_Initiate internal state machine state to UNEXPECTED PACKET Received error */
        DCPInternalData.TXState = DCP_SM_STEP6;

        /* DCP Module Idle state */
        DCPInternalData.DCPCoreState = DCP_SM_START;
      }
    }
    /* Check if Timeout already elapsed */
    else if (DCP_HW_TimerElapsed() == TRUE)
    {
      /* ACK timeout error */

      /* Change the DCP_Initiate internal state machine state to ACK Timeout error */
      DCPInternalData.TXState = DCP_SM_STEP2;

      /* Reset state of receiver */
      DCP_IF_RecvPacket(1u);

      /* DCP Module Idle state */
      DCPInternalData.DCPCoreState = DCP_SM_START;
    }

    /* Clear TX initiated flag */
    DCPInternalData.Flags &= ~(1u << DCP_TX_INITIATED);
    break;

  case DCP_SM_STEP4:
    /* Waiting DCP Indication message */

    if (DCP_IF_RecvPacket(0u) == TRUE)
    {
      /* Is EDC Ok? */
      if (DCPInternalData.Flags & (1u << DCP_RX_WRONGEDC))
      {
        /* Change the DCP_Initiate internal state machine state to RX EDC error */
        DCPInternalData.TXState = DCP_SM_STEP5;

        /* DCP Module Idle state */
        DCPInternalData.DCPCoreState = DCP_SM_START;
      }
      /* Check if it is IND */
      else if (DCP_GetMessageType(DCPRXBuffer) == (eDCPMessageTypes) MSG_INDICATION)
      {
        /* IND received */

        /* Send an ACK back */
        DCP_SendAck();

        /* Change the DCP_Initiate internal state machine state to OK */
        DCPInternalData.TXState = DCP_SM_STEP7;

        /* Indication DCP packet is in DCPRXBuffer */
        DCPInternalData.DCPCoreState = DCP_SM_START;
      }
      else
      {
        /* UNEXPECTED PACKET received */

        /* Change the DCP_Initiate internal state machine state to UNEXPECTED PACKET Received error */
        DCPInternalData.TXState = DCP_SM_STEP6;

        /* DCP Module Idle state */
        DCPInternalData.DCPCoreState = DCP_SM_START;
      }
    }
    /* Check if Timeout already elapsed */
    else if (DCP_HW_TimerElapsed() == TRUE)
    {
      /* Indication timeout error */

      /* Change the DCP_Initiate internal state machine state to IND Timeout error */
      DCPInternalData.TXState = DCP_SM_STEP4;

      /* Reset state of receiver */
      DCP_IF_RecvPacket(1u);

      /* DCP Module Idle state */
      DCPInternalData.DCPCoreState = DCP_SM_START;
    }

    break;

  default:
    /* Go to Idle State */

    DCPInternalData.DCPCoreState = DCP_SM_START;

    break;
  }
}

/**
 *  Initiates a DCP session. The data and command set is given by function
 *  parameters, the Sync Counter will be the internal SyncCounter + 1u. <br>
 *  The data received by indication message will be placed into DCPRXBuffer.
 *  The data buffer pointed by pTxBuffer must hold the value until the DCP
 *  Initiation returns other than DCP_RES_BUSY!
 *
 *  @param[in] CmdSet     DCP Request Command Set
 *  @param[in] Length     Length of data to be sent (without headers)
 *  @param[in] *pTxBuffer Pointer to data to be sent
 *
 *  @return Result of DCP Initiation
 *
 *  @note   Must be called periodically until return differs than DCP_RES_BUSY.
 *
 *  @note   <b> The internal SM state can be changed outside the function
 *          (eg. in DCP_Pollhandler) according to operation outcome! </b>
 *
 *  @author Sz. Papp
 *
 *****************************************************************************/
eDCPInitiateResults DCP_Initiate(U8 CmdSet, U8 Length, U8 *pTxBuffer)
{
  eDCPInitiateResults retVal = (eDCPInitiateResults) DCP_RES_BUSY;

  switch (DCPInternalData.TXState)
  {
  case DCP_SM_START:
    /* Set TX data pointer & Command Set & Length */
    DCPInternalData.pTxData = pTxBuffer;
    DCPInternalData.TXCommandSet = CmdSet;
    DCPInternalData.TXLength = Length;

    /* Set DCP transmit scheduled flag */
    DCPInternalData.Flags |= (1u << DCP_TX_INITIATED);

    DCPInternalData.TXState = DCP_SM_STEP1;

    break;

  case DCP_SM_STEP1:
    /* Waiting to result of transmission */

    break;

  case DCP_SM_STEP2:
    /* ACK timeout error */

    #ifdef DCP_ACK_TIMEOUT_CALLBACK_ENABLE
    DCP_AckTimeout_Callback();
    #endif

    DCPInternalData.TXState = DCP_SM_START;

    retVal = DCP_RES_TIMEOUT_ACK;
    break;

  case DCP_SM_STEP3:
    /* NotACK Received error */

    #ifdef DCP_NOTACK_RECEIVED_CALLBACK_ENABLE
    DCP_NotAckReceived_Callback();
    #endif

    DCPInternalData.TXState = DCP_SM_START;

    retVal = DCP_RES_NOTACK_ERROR;
    break;

  case DCP_SM_STEP4:
    /* IND timeour error */

    #ifdef DCP_INDICATION_TIMEOUT_CALLBACK_ENABLE
    DCP_IndicationTimeoutCallback();
    #endif

    DCPInternalData.TXState = DCP_SM_START;

    retVal = DCP_RES_TIMEOUT_IND;
    break;

  case DCP_SM_STEP5:
    /* RX EDC error */

    /* Callback already invoked by IF function */

    DCPInternalData.TXState = DCP_SM_START;

    retVal = DCP_RES_EDC_ERROR;
    break;

  case DCP_SM_STEP6:
    /* UNEXPECTED PACKET received */

    #ifdef DCP_UNEXPECTED_PACKET_RECEIVED_CALLBACK_ENABLE
    DCP_UnxpctdPcktRcvd_Callback();
    #endif

    DCPInternalData.TXState = DCP_SM_START;

    retVal = DCP_RES_UNEXPECTED_PACKET_ERROR;
    break;

  case DCP_SM_STEP7:
    /* DCP Transmission is OK */

    #ifdef DCP_INITIATE_FINISHED_CALLBACK_ENABLE
    void DCP_InitiateFinished_Callback();
    #endif

    DCPInternalData.TXState = DCP_SM_START;

    retVal = DCP_RES_OK;
    break;

  default:
    break;
  }

  return retVal;
}

/**
 *  Send a Not Acknowledge DCP message.
 *
 *  @param[in]  e Cause of Not Ack
 *
 *  @return     State of transmission / \b TRUE - Transmitted | \b FALSE -
 *              Not yet transmitted /
 *
 *  @remarks    Modify the content of global DCPTXBuffer variable. <br>
 *              The DCPRXBuffer must hold the actual DCP Request this \b NotACK
 *              responses for.
 *
 *  @note       Must be called periodically.
 *
 *  @author     Sz. Papp
 *
 *****************************************************************************/
U8 DCP_SendNotAck(eNotACKs e)
{
  /* DCP Not Acknowledge message compilation */
  DCPTXBuffer[0u] = 0x55;
  DCPTXBuffer[1u] = 3u;

  /* Already holds the CommandSet and Sync Counter, MsgType to NotAck */
  DCPTXBuffer[2u] = (DCPRXBuffer[2u] & 0x3F) | 0x80;

  switch (e)
  {
  case NOTACK_EDC_ERROR:
    DCPTXBuffer[3u] = 0x00;
    break;

  case NOTACK_COMMAND_SET_NOT_SUPPORTED:
    DCPTXBuffer[3u] = 0x01;
    break;

  case NOTACK_OUT_OF_SYNC:
    DCPTXBuffer[3u] = 0x02;
    break;

  case NOTACK_UNEXPECTED:
    DCPTXBuffer[3u] = 0x03;
    break;

  default:
    /* General Error */
    DCPTXBuffer[3u] = 0xFF;
    break;
  }

  /* Calc EDC */
  DCPTXBuffer[4u] = DCP_IF_CalcEDC(DCPTXBuffer);

  /* Send the DCP packet */
  DCP_IF_SendPacket(DCPTXBuffer);

  return TRUE;
}

/**
 *  Send an Acknowledge DCP message.
 *
 *  @return   State of transmission / \b TRUE - Transmitted | \b FALSE -
 *              Not yet transmitted /
 *
 *  @remarks  Modify the content of global DCPTXBuffer variable. <br>
 *            The DCPRXBuffer must hold the actual DCP Request this \b ACK
 *            responses for.
 *
 *  @note     Must be called periodically.
 *
 *  @author   Sz. Papp
 *
 *****************************************************************************/
U8 DCP_SendAck(void)
{
  /* DCP Acknowledge message compilation */
  DCPTXBuffer[0u] = 0x55;
  DCPTXBuffer[1u] = 2u;

  /* Already holds the CommandSet and Sync Counter, MsgType to Ack */
  DCPTXBuffer[2u] = (DCPRXBuffer[2u] & 0x3F) | 0x40;

  /* Calc EDC */
  DCPTXBuffer[3u] = DCP_IF_CalcEDC(DCPTXBuffer);

  /* Send the DCP packet */
  DCP_IF_SendPacket(DCPTXBuffer);

  return TRUE;
}

/**
 *  Returns the given DCP packet Message Type.
 *
 *  @param[in]  p Pointer to DCP packet.
 *
 *  @return     Type of Message
 *
 *  @author     Sz. Papp
 *
 *****************************************************************************/
eDCPMessageTypes DCP_GetMessageType(U8 * p)
{
  switch (*(p + 2u) & 0xC0)
  {
  case 0x00:
    return MSG_REQUEST;
    break;

  case 0x40:
    return MSG_ACKNOWLEDGE;
    break;

  case 0x80:
    return MSG_NOTACKNOWLEDGE;
    break;

  default:
    break;
  }

  return MSG_INDICATION;
}


