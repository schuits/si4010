/**
 *  @file DCPCommand.h
 *  @brief This header file contains the function and variable declarations for
 *          the enabled DCP Command Set handler functions. It contains related
 *          type definitions and macros too.
 *
 *  @author Sz. Papp
 *
 *  @date 02/27/2012
 *
 */

/*
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef DCPCOMMAND_H_
#define DCPCOMMAND_H_

/*****************************************************************************
 *  Global Enums & Typedefs
 *****************************************************************************/

/*! Enum defines the DCP General Command Set subcommands */
typedef enum
{
GENERAL_CS_INDICATION,
GENERAL_CS_GET_DEVICE_INFO_REQ,
GENERAL_CS_DEVICE_INFO_IND,
GENERAL_CS_BOOTLOADER_MODE_REQ
};

/*! Enum defines the RFStick EZRadio2 Lab Measurement Command Set subcommands */
typedef enum
{
  LAB_MEASURE_CS_GENERAL_INDICATION,
  LAB_MEASURE_CS_SPI_COMMAND_REQ                = 0x10,
  LAB_MEASURE_CS_SPI_COMMAND_IND,
  LAB_MEASURE_CS_SMBUS_COMMAND_REQ              = 0x27,
  LAB_MEASURE_CS_SMBUS_READ_REQ,
  LAB_MEASURE_CS_SMBUS_IND,
  LAB_MEASURE_CS_SET_IO_DIRECTION_REQ           = 0x12,
  LAB_MEASURE_CS_SET_IO_OUTPUT_REQ,
  LAB_MEASURE_CS_GET_IO_INPUT_REQ,
  LAB_MEASURE_CS_GET_IO_VALUE_IND,
  LAB_MEASURE_CS_CONTROL_SPI_PERIPHERAL_REQ,
  LAB_MEASURE_CS_CONTROL_SMBUS_PERIPHERAL_REQ,
  LAB_MEASURE_CS_CONTROL_LEDS_REQ,
  LAB_MEASURE_CS_LCD_WRITELINE_REQ,
  LAB_MEASURE_CS_GET_PUSHBUTTONS_STATE_REQ,
  LAB_MEASURE_CS_GET_PUSHBUTTONS_STATE_IND,
  LAB_MEASURE_CS_SYSCLOCK_VALUE_IND             = 0x2C,
  LAB_MEASURE_CS_GET_SYSCLOCK_REQ,
  LAB_MEASURE_CS_SET_SPI_SPEED_REQ,
/*  LAB_MEASURE_CS_SET_SYSCLOCK_REQ,                        //TODO */
  LAB_MEASURE_CS_SET_BUZZER_REQ                 = 0x33,
  LAB_MEASURE_CS_LCD_WRITERAM_REQ               = 0x35,
  LAB_MEASURE_CS_LCD_CLEAR_REQ
};

/*! Enum defines RFStick EZRadio2 Lab Measurement Command Set subIndications */
typedef enum
{
  LAB_MEASURE_CS_REQ_COMPLETE,
  LAB_MEASURE_CS_COMMAND_NOT_SUPPORTED,
  LAB_MEASURE_CS_INVALID_PARAMETER,
  LAB_MEASURE_CS_DEVICE_BUSY,
  LAB_MEASURE_CS_HW_PERIPHERAL_USED,
  LAB_MEASURE_CS_HW_PERIPHERAL_DISABLED
};

/*! Struct declaration for array that holds supported Command Set and its
 * appropriate handler function. */
typedef struct
{
  U8 CmdSet;
  U8 (*CmdHandler)(U8 *);
} tCommandSets;

/*! Sturct declaration to be able to handle the DCP packet common header by
 * pointer. */
typedef struct
{
  U8 StartOfFrame;
  U8 Length;
  U8 Control;
  U8 subCommand;
  U8 DataStart;
  /* DATA */
  /* EDC  */
} tGeneralCommandSet;

/*****************************************************************************
 *  Global Macros & Definitions
 *****************************************************************************/

/*! Supported Command sets */
#define COMMAND_SET_GENERAL                 0x00
#define COMMAND_SET_LAB_MEASURE             0x07

/*! Supported subCommands */

/* General Command Set */
#define GENERAL_CS_COMMAND_NOT_SUPPORTED    0x01

/* RFSTick EZRadio2 Lab Measurement Command Set */

/*****************************************************************************
 *  Global Variable Definitions
 *****************************************************************************/

/*! Supported DCP2 Command Sets global array */
extern const SEGMENT_VARIABLE(DCP_SupportedCommandSets[], tCommandSets, SEG_CODE);

/*****************************************************************************
 *  Global Function Definitions
 *****************************************************************************/
U8 DCP_CommandSetSupported(U8 *);


#endif /* DCPCOMMAND_H_ */
