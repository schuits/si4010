/**
 *  Copyright 2008 Silicon Laboratories, Inc.
 *  http://www.silabs.com
 *
 *  @file SoftTimer.h
 *  
 *  C File Description:
 *  @brief TODO
 *
 *  Project Name: dev_EzR2_Loadboard 
 * 
 * 
 *  @author Sz. Papp
 *
 *  @date 2012.03.07.
 *
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 *  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *  This software must be used in accordance with the End User License Agreement.
 */

#ifndef SOFTTIMER_H_
#define SOFTTIMER_H_

/*****************************************************************************
 *  Global Enums & Typedefs
 *****************************************************************************/
typedef enum
{
  SOFTTIMER_CH0,
  SOFTTIMER_CH1,
  SOFTTIMER_CH2,
  SOFTTIMER_CH3,
  SOFTTIMER_NUMOF_CHANNELS
} eSoftTimerChannels;

typedef struct
{
  U16 Ticks;
  U8  Elapsed;
} tSoftTimer;

/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
void SoftTimer_Add(U16, eSoftTimerChannels);
void SoftTimer_Remove(eSoftTimerChannels);
U8   SoftTimer_Elapsed(eSoftTimerChannels);

void StartTimer3(U16);
void StopTimer3(void);

INTERRUPT_PROTO(Timer2ISR, INTERRUPT_TIMER2);
INTERRUPT_PROTO(Timer3ISR, INTERRUPT_TIMER3);

/*****************************************************************************
 *  Global Variables Declarations
 *****************************************************************************/
extern SEGMENT_VARIABLE(SoftTimer_Channels[SOFTTIMER_NUMOF_CHANNELS], tSoftTimer, SEG_XDATA);


#endif /* SOFTTIMER_H_ */
