/**
 *  Copyright 2008 Silicon Laboratories, Inc.
 *  http://www.silabs.com
 *
 *  @file CommIF.h
 *  
 *  C File Description:
 *  @brief Communication interface functions for the EzRadio2 Loadboard.
 *
 *  Project Name: EzRadio_Si4455
 * 
 * 
 *  @author Sz. Papp
 *
 *  @date 2012.03.02.
 *
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 *  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *  This software must be used in accordance with the End User License Agreement.
 */

#ifndef COMMIF_H_
#define COMMIF_H_

/*****************************************************************************
 *  Global Macros & Definitions
 *****************************************************************************/

/*! UART RX Buffer size */
#define COMM_IF_UART_RX_BUFFER  64u

/*! UART TX Buffer size */
#define COMM_IF_UART_TX_BUFFER  64u

/*! Macro to set th UART baud rate to 115200bps */
#define Set115200bps_24MHZ5     CKCON &= 0xF4; CKCON |=  0x08; \
                                TH1    = 0x96; TMOD  &= ~0xF0; \
                                TMOD  |= 0x20

#define DELAY_1US()             for (wDelay = 0x01FF; wDelay--; )

/*****************************************************************************
 *  Global Enums & Typedefs
 *****************************************************************************/

typedef enum
{
  SMBUS_TRANSMISSION_OK,
  SMBUS_RX_FINISHED,
  SMBUS_ARBITRATION_LOST,
  SMBUS_TIMEOUT_ERROR,
  SMBUS_BUSY,
  SMBUS_WRONG_SLAVE_ADDRESS,
  SMBUS_NACK_RECEIVED,
  SMBUS_ACK_RECEIVED,
  SMBUS_GENERAL_ERROR
} eSMBusReturnStates;

typedef struct
{
  U8  TXWritePosition;
  U8  TXReadPosition;
  U8  TXBufferEmpty;
  U8  RXWritePosition;
  U8  RXReadPosition;
  U8  RXBuffer[COMM_IF_UART_RX_BUFFER];
  U8  TXBuffer[COMM_IF_UART_TX_BUFFER];
} tUartData;

/*****************************************************************************
 *  Global Variable Declarations
 *****************************************************************************/
extern SEGMENT_VARIABLE(fSelectState,         U8, SEG_DATA);
extern SEGMENT_VARIABLE(fSPIDisabled,         U8, SEG_DATA);
extern SEGMENT_VARIABLE(fSPIBusy,             U8, SEG_DATA);
extern SEGMENT_VARIABLE(fSMBusTransaction,    U8, SEG_DATA);

/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
U8 Comm_IF_RecvUART(U8 *);
U8 Comm_IF_SendUART(U8);

void Comm_IF_EnableUART(void);

U8 Comm_IF_Spi1ReadWrite(U8);
U8 Comm_IF_Spi0ReadWrite(U8);

U8 Comm_IF_Spi1ReadWrite_Byte(U8);
U16 Comm_IF_Spi1ReadWrite_Word(U8, U8);
void Comm_IF_Spi1ReadWrite_Burst(U8, U8, U8 *, U8 *);

U8 Comm_IF_Spi1ReadByteBitbang(void);
void Comm_IF_Spi1WriteBitsBitbang(U8, U8);

U8 Comm_IF_Spi1DebugRead(U8, U8, U8);
U8 Comm_IF_Spi1DebugReadSFR(U8, U8, U8);

void Comm_IF_Spi1DebugWrite(U8, U8, U8, U8);
void Comm_IF_Spi1DebugWriteSFR(U8, U8, U8, U8);

void Comm_IF_Spi1Enable(void);
void Comm_IF_Spi1Disable(void);

void Comm_IF_SpiClearNsel(U8);
void Comm_IF_SpiSetNsel(U8);

void Comm_IF_InitSMBusInterface(void);
void Comm_IF_DisableSMBusInterface(void);

//TODO: may be local functions

void Comm_IF_SMBusStart(void);
void Comm_IF_SMBusStop(void);

eSMBusReturnStates Comm_IF_SMBusWriteByte(U8 Data);
U8 Comm_IF_SMBusReadByte(U8 AckReq);

eSMBusReturnStates Comm_IF_SMBusWrite(U8 Address, U8 Length, U8 * pData);
eSMBusReturnStates Comm_IF_SMBusRead(U8 Address, U8 Length, U8 * pData);
U8 Comm_IF_SMBusWaitForItWithTimeout(void);

#endif /* COMMIF_H_ */
