cd DCPModule
call Cleanup.bat
cd ..
cd Radio
call Cleanup.bat
cd ..
REM KEIL files
del *.#1
del *.#2
del *.#3
del *.lst
del *.m51
del cyglink.txt
del *.obj
del *.omf
del tmp.out
del *.backup
del *.Bak
del *.__i
del *.map
del *.i
del *.sbr
del *.plg
REM SDCC files
del *.sym
del *.rel
del *.rst
del *.adb
del *.mem
del *.cdb
del *.lk
del *.asm
REM pause
