/*!
 * File:
 *  radio_hal_ext0.h
 *
 * Description:
 *  This file contains RADIO HAL for External0 INT context.
 *
 * Silicon Laboratories Confidential
 * Copyright 2011 Silicon Laboratories, Inc.
 */

#ifndef _RADIO_HAL_EXT0_H_
#define _RADIO_HAL_EXT0_H_

                /* ======================================= *
                 *              I N C L U D E              *
                 * ======================================= */

                /* ======================================= *
                 *          D E F I N I T I O N S          *
                 * ======================================= */

                /* ======================================= *
                 *     G L O B A L   V A R I A B L E S     *
                 * ======================================= */

                /* ======================================= *
                 *  F U N C T I O N   P R O T O T Y P E S  *
                 * ======================================= */

BIT radio_hal_Gpio0Level_ext0(void);
BIT radio_hal_Gpio1Level_ext0(void);
BIT radio_hal_Gpio2Level_ext0(void);
BIT radio_hal_Gpio3Level_ext0(void);
BIT radio_hal_NirqLevel_ext0(void);
void radio_hal_AssertShutdown_ext0(void);
void radio_hal_DeassertShutdown_ext0(void);
void radio_hal_ClearNsel_ext0(void);
void radio_hal_SetNsel_ext0(void);
void radio_hal_SpiWriteByte_ext0(U8 byteToWrite);
U8 radio_hal_SpiReadByte_ext0(void);
void radio_hal_SpiWriteData_ext0(U8 byteCount, U8* pData);
void radio_hal_SpiReadData_ext0(U8 byteCount, U8* pData);


#endif //_RADIO_HAL_EXT0_H_
