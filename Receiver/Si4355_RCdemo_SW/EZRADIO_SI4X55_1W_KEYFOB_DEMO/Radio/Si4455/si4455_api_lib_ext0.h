/*!
 * File:
 *  si4455_api_lib_ext0.h
 *
 * Description:
 *  This file contains the Si4455 API library for External0 context.
 *
 * Silicon Laboratories Confidential
 * Copyright 2011 Silicon Laboratories, Inc.
 */

#ifndef _SI4455_API_LIB_EXT0_H_
#define _SI4455_API_LIB_EXT0_H_

void si4455_reset_ext0(void);
void si4455_nop_ext0(void);
void si4455_part_info_ext0(void);
void si4455_power_up_ext0(U8 BOOT_OPTIONS, U8 XTAL_OPTIONS, U32 XO_FREQ);

void si4455_func_info_ext0(void);
void si4455_set_property_ext0( U8 GROUP, U8 NUM_PROPS, U8 START_PROP, ... );
void si4455_get_property_ext0(U8 GROUP, U8 NUM_PROPS, U8 START_PROP);
void si4455_gpio_pin_cfg_ext0(U8 GPIO0, U8 GPIO1, U8 GPIO2, U8 GPIO3, U8 NIRQ, U8 SDO, U8 GEN_CONFIG);
void si4455_fifo_info_ext0(U8 FIFO);
void si4455_ezconfig_check_ext0(UU16 CHECKSUM);

void si4455_get_int_status_ext0(U8 PH_CLR_PEND, U8 MODEM_CLR_PEND, U8 CHIP_CLR_PEND);

void si4455_start_tx_ext0(U8 CHANNEL, U8 CONDITION, U16 TX_LEN);
void si4455_start_rx_ext0(U8 CHANNEL, U8 CONDITION, U16 RX_LEN, U8 NEXT_STATE1, U8 NEXT_STATE2, U8 NEXT_STATE3);
void si4455_request_device_state_ext0(void);
void si4455_change_state_ext0(U8 NEXT_STATE1);
void si4455_read_cmd_buff_ext0(void);
void si4455_frr_a_read_ext0(U8 respByteCount);
void si4455_frr_b_read_ext0(U8 respByteCount);
void si4455_frr_c_read_ext0(U8 respByteCount);
void si4455_frr_d_read_ext0(U8 respByteCount);

void si4455_write_tx_fifo_ext0(U8 numBytes, U8* pData);
void si4455_read_rx_fifo_ext0(U8 numBytes, U8* pRxData);

#endif //_SI4455_API_LIB_EXT0_H_
