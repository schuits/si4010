/*!
 * File:
 *  radio_comm_ext0.h
 *
 * Description:
 *  This file contains the RADIO communication layer for External0 INT context.
 *
 * Silicon Laboratories Confidential
 * Copyright 2011 Silicon Laboratories, Inc.
 */
#ifndef _RADIO_COMM_EXT0_H_
#define _RADIO_COMM_EXT0_H_


                /* ======================================= *
                 *              I N C L U D E              *
                 * ======================================= */

                /* ======================================= *
                 *          D E F I N I T I O N S          *
                 * ======================================= */

//#define RADIO_CTS_TIMEOUT 255
#define RADIO_CTS_TIMEOUT 5000


                /* ======================================= *
                 *     G L O B A L   V A R I A B L E S     *
                 * ======================================= */

extern SEGMENT_VARIABLE(radioCmd[16u], U8, SEG_XDATA);


                /* ======================================= *
                 *  F U N C T I O N   P R O T O T Y P E S  *
                 * ======================================= */

U8 radio_comm_GetResp_ext0(U8 byteCount, U8* pData);
U8 radio_comm_PollCTS_ext0(void);
void radio_comm_SendCmd_ext0(U8 byteCount, U8* pData);
U8 radio_comm_SendCmdGetResp_ext0(U8 cmdByteCount, U8* pCmdData, U8 respByteCount, U8* pRespData);
void radio_comm_ReadData_ext0(U8 cmd, BIT pollCts, U8 byteCount, U8* pData);
void radio_comm_WriteData_ext0(U8 cmd, BIT pollCts, U8 byteCount, U8* pData);


#endif //_RADIO_COMM_EXT0_H_
