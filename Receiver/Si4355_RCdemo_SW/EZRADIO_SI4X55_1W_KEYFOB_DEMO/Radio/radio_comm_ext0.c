/*!
 * File:
 *  radio_comm_ext0.c
 *
 * Description:
 *  This file contains the RADIO communication layer for External0 INT context.
 *
 * Silicon Laboratories Confidential
 * Copyright 2011 Silicon Laboratories, Inc.
 */

                /* ======================================= *
                 *              I N C L U D E              *
                 * ======================================= */

#include "..\..\bsp.h"

                /* ======================================= *
                 *          D E F I N I T I O N S          *
                 * ======================================= */

                /* ======================================= *
                 *     G L O B A L   V A R I A B L E S     *
                 * ======================================= */

BIT ctsWentHigh_ext0 = 0;


                /* ======================================= *
                 *      L O C A L   F U N C T I O N S      *
                 * ======================================= */

                /* ======================================= *
                 *     P U B L I C   F U N C T I O N S     *
                 * ======================================= */

/*!
 * Gets a command response from the radio chip
 *
 * @param byteCount     Number of bytes to get from the radio chip
 * @param pData         Pointer to where to put the data
 *
 * @return CTS value
 */
U8 radio_comm_GetResp_ext0(U8 byteCount, U8* pData)
{
    SEGMENT_VARIABLE(ctsVal = 0, U8, SEG_DATA);
    SEGMENT_VARIABLE(errCnt = RADIO_CTS_TIMEOUT, U16, SEG_DATA);

    while(errCnt != 0)      //wait until radio IC is ready with the data
    {
        radio_hal_ClearNsel_ext0();
        radio_hal_SpiWriteByte_ext0(0x44);    //read CMD buffer
        ctsVal = radio_hal_SpiReadByte_ext0();
        if(ctsVal == 0xFF)
        {
            if(byteCount)
            {
                radio_hal_SpiReadData_ext0(byteCount, pData);
            }
            radio_hal_SetNsel_ext0();
            break;
        }
        radio_hal_SetNsel_ext0();
        errCnt--;
    }

    if( errCnt == 0 )
    {
        while(1)
        {
            /* ERROR!!!!  CTS should never take this long. */
        }
    }

    if(ctsVal == 0xFF)
    {
        ctsWentHigh_ext0 = 1;
    }

    return ctsVal;
}



/*!
 * Waits for CTS to be high
 *
 * @return CTS value
 */
U8 radio_comm_PollCTS_ext0(void)
{
#ifdef RADIO_USER_CFG_USE_GPIO1_FOR_CTS
    while(!radio_hal_Gpio1Level_ext0())
    {
        /* Wait...*/
    }
    ctsWentHigh_ext0 = 1;
    return 0xFF;
#else
    return radio_comm_GetResp_ext0(0, 0);
#endif
}

/*!
 * Sends a command to the radio chip
 *
 * @param byteCount     Number of bytes in the command to send to the radio device
 * @param pData         Pointer to the command to send.
 */
void radio_comm_SendCmd_ext0(U8 byteCount, U8* pData)
{
    /* There was a bug in A1 hardware that will not handle 1 byte commands. 
       It was supposedly fixed in B0 but the fix didn't make it at the last minute, so here we go again */
    if(byteCount == 1)
        byteCount++;

    while(!ctsWentHigh_ext0)
    {
        radio_comm_PollCTS_ext0();
    }
    radio_hal_ClearNsel_ext0();
    radio_hal_SpiWriteData_ext0(byteCount, pData);
    radio_hal_SetNsel_ext0();
    ctsWentHigh_ext0 = 0;
}


/*!
 * Sends a command to the radio chip and gets a response
 *
 * @param cmdByteCount  Number of bytes in the command to send to the radio device
 * @param pCmdData      Pointer to the command data
 * @param respByteCount Number of bytes in the response to fetch
 * @param pRespData     Pointer to where to put the response data
 *
 * @return CTS value
 */
U8 radio_comm_SendCmdGetResp_ext0(U8 cmdByteCount, U8* pCmdData, U8 respByteCount, U8* pRespData)
{
    radio_comm_SendCmd_ext0(cmdByteCount, pCmdData);
    return radio_comm_GetResp_ext0(respByteCount, pRespData);
}


/*!
 * Gets a command response from the radio chip
 *
 * @param cmd           Command ID
 * @param pollCts       Set to poll CTS
 * @param byteCount     Number of bytes to get from the radio chip.
 * @param pData         Pointer to where to put the data.
 */
void radio_comm_ReadData_ext0(U8 cmd, BIT pollCts, U8 byteCount, U8* pData)
{
    if(pollCts)
    {
        while(!ctsWentHigh_ext0)
        {
            radio_comm_PollCTS_ext0();
        }
    }
    radio_hal_ClearNsel_ext0();
    radio_hal_SpiWriteByte_ext0(cmd);
    radio_hal_SpiReadData_ext0(byteCount, pData);
    radio_hal_SetNsel_ext0();
    ctsWentHigh_ext0 = 0;
}


/*!
 * Gets a command response from the radio chip
 *
 * @param cmd           Command ID
 * @param pollCts       Set to poll CTS
 * @param byteCount     Number of bytes to get from the radio chip
 * @param pData         Pointer to where to put the data
 */
void radio_comm_WriteData_ext0(U8 cmd, BIT pollCts, U8 byteCount, U8* pData)
{
    if(pollCts)
    {
        while(!ctsWentHigh_ext0)
        {
            radio_comm_PollCTS_ext0();
        }
    }
    radio_hal_ClearNsel_ext0();
    radio_hal_SpiWriteByte_ext0(cmd);
    radio_hal_SpiWriteData_ext0(byteCount, pData);
    radio_hal_SetNsel_ext0();
    ctsWentHigh_ext0 = 0;
}


