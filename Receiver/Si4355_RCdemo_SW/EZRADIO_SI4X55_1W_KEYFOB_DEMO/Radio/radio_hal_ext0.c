/*!
 * File:
 *  radio_hal.c
 *
 * Description:
 *  This file contains RADIO HAL.
 *
 * Silicon Laboratories Confidential
 * Copyright 2011 Silicon Laboratories, Inc.
 */

                /* ======================================= *
                 *              I N C L U D E              *
                 * ======================================= */

#include "..\..\bsp.h"


                /* ======================================= *
                 *          D E F I N I T I O N S          *
                 * ======================================= */

                /* ======================================= *
                 *     G L O B A L   V A R I A B L E S     *
                 * ======================================= */

                /* ======================================= *
                 *      L O C A L   F U N C T I O N S      *
                 * ======================================= */

                /* ======================================= *
                 *     P U B L I C   F U N C T I O N S     *
                 * ======================================= */

BIT radio_hal_Gpio0Level_ext0(void)
{
    #ifdef SILABS_PLATFORM_DKMB
    return 0;
    #endif
    #ifdef SILABS_PLATFORM_UDP
    return EZRP_RX_DOUT;
    #endif
    #ifdef SILABS_PLATFORM_RFSTICK
    return RF_GPIO0;
    #endif
}

BIT radio_hal_Gpio1Level_ext0(void)
{
    #ifdef SILABS_PLATFORM_DKMB
    return FSK_CLK_OUT;
    #endif
    #ifdef SILABS_PLATFORM_UDP
    return 0; //No Pop
    #endif
    #ifdef SILABS_PLATFORM_RFSTICK
    return RF_GPIO1;
    #endif
}

BIT radio_hal_Gpio2Level_ext0(void)
{
    #ifdef SILABS_PLATFORM_DKMB
    return DATA_NFFS;
    #endif
    #ifdef SILABS_PLATFORM_UDP
    return 0; //No Pop
    #endif
    #ifdef SILABS_PLATFORM_RFSTICK
    return RF_GPIO2;
    #endif
}

BIT radio_hal_Gpio3Level_ext0(void)
{
    #ifdef SILABS_PLATFORM_RFSTICK
    return RF_GPIO3;
    #else
    return 0;
    #endif
}

BIT radio_hal_NirqLevel_ext0(void)
{
    return RF_NIRQ;
}

void radio_hal_AssertShutdown_ext0(void)
{
    #ifdef SILABS_PLATFORM_RFSTICK
    RF_PWRDN = 1;
    #else
    PWRDN = 1;
    #endif
}

void radio_hal_DeassertShutdown_ext0(void)
{
    #ifdef SILABS_PLATFORM_RFSTICK
    RF_PWRDN = 0;
    #else
    PWRDN = 0;
    #endif
}

void radio_hal_ClearNsel_ext0(void)
{
    RF_NSEL = 0;
}

void radio_hal_SetNsel_ext0(void)
{
    RF_NSEL = 1;
}

void radio_hal_SpiWriteByte_ext0(U8 byteToWrite)
{
    #ifdef SILABS_PLATFORM_RFSTICK
    bSpi_ReadWriteSpi1(byteToWrite);
    #else
    SpiReadWrite(byteToWrite);
    #endif
}

U8 radio_hal_SpiReadByte_ext0(void)
{
    #ifdef SILABS_PLATFORM_RFSTICK
    return bSpi_ReadWriteSpi1(0xFF);
    #else
    return SpiReadWrite(0xFF);
    #endif
}

void radio_hal_SpiWriteData_ext0(U8 byteCount, U8* pData)
{
    #ifdef SILABS_PLATFORM_RFSTICK
    vSpi_WriteDataSpi1(byteCount, pData);
    #else
    SpiWriteData(byteCount, pData);
    #endif
}

void radio_hal_SpiReadData_ext0(U8 byteCount, U8* pData)
{
    #ifdef SILABS_PLATFORM_RFSTICK
    vSpi_ReadDataSpi1(byteCount, pData);
    #else
    SpiReadData(byteCount, pData);
    #endif
}

