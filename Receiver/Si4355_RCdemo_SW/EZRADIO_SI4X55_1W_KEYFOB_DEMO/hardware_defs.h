/**
 *  Copyright 2008 Silicon Laboratories, Inc.
 *  http://www.silabs.com
 *
 *  @file hardware_defs.h
 *  
 *  C File Description:
 *  @brief TODO
 *
 *  Project Name: dev_EzR2_Loadboard 
 * 
 * 
 *  @author Sz. Papp
 *
 *  @date 2012.03.02.
 *
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 *  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *  This software must be used in accordance with the End User License Agreement.
 */

#ifndef HARDWARE_DEFS_H_
#define HARDWARE_DEFS_H_

/*****************************************************************************
 *  Global Macros & Definitions
 *****************************************************************************/

/*! System Clock */
#define SYS_CLK_KHZ            24500   /* 24.5 MHz, Precision */

/*! Critical Section Macros */
#define ENTER_CRITICAL_SECTION    EA = 0
#define EXIT_CRITICAL_SECTION     EA = 1

/*! Hardware related definitions */
#define HW_NUMOF_LEDS             4u

#define PIN_MCU_MOSI              2u
#define PORT_MCU_MOSI             P1
#define PXMDOUT_MCU_MOSI     P1MDOUT

#define PIN_MCU_MISO              1u
#define PORT_MCU_MISO             P1
#define PXMDOUT_MCU_MISO     P1MDOUT

#define PIN_MCU_SCK               0u
#define PORT_MCU_SCK              P1
#define PXMDOUT_MCU_SCK      P1MDOUT

#define PIN_MCU_NSEL              3u
#define PORT_MCU_NSEL             P1
#define PXMDOUT_MCU_NSEL     P1MDOUT

#define PIN_MCU_SDA               6u
#define PORT_MCU_SDA              P0
#define PXMDOUT_MCU_SDA      P0MDOUT

#define PIN_MCU_SCL               7u
#define PORT_MCU_SCL              P0
#define PXMDOUT_MCU_SCL      P0MDOUT

#define PIN_MCU_GPIO0             0u
#define PORT_MCU_GPIO0            P0
#define PXMDOUT_MCU_GPIO0    P0MDOUT

#define PIN_MCU_GPIO1             1u
#define PORT_MCU_GPIO1            P0
#define PXMDOUT_MCU_GPIO1    P0MDOUT

#define PIN_MCU_GPIO2             2u
#define PORT_MCU_GPIO2            P0
#define PXMDOUT_MCU_GPIO2    P0MDOUT

#define PIN_MCU_GPIO3             3u
#define PORT_MCU_GPIO3            P0
#define PXMDOUT_MCU_GPIO3    P0MDOUT

#define PIN_MCU_SDN               5u
#define PORT_MCU_SDN              P1
#define PXMDOUT_MCU_SDN      P1MDOUT

#define PIN_MCU_NIRQ              4u
#define PORT_MCU_NIRQ             P1
#define PXMDOUT_MCU_NIRQ     P1MDOUT


#define SILABS_PLATFORM_RFSTICK
#define SILABS_CUSTOM_SPI_DRIVER

/*****************************************************************************
 *  Global Bit Definitions
 *****************************************************************************/

/* SMBus */
SBIT(MCU_SDA,  SFR_P0, 7);
SBIT(MCU_SCL,  SFR_P0, 6);

/* LEDs */
SBIT(LED1,     SFR_P2, 0);
SBIT(LED2,     SFR_P2, 1);
SBIT(LED3,     SFR_P2, 2);
SBIT(LED4,     SFR_P2, 3);

/* Buzzer */
SBIT(BUZZ,     SFR_P2, 4);

/* MCU SPI1 */
SBIT(MCU_SCK,  SFR_P1, 0);
SBIT(MCU_MISO, SFR_P1, 1);
SBIT(MCU_MOSI, SFR_P1, 2);

/* RF related */
SBIT(RF_NSEL,  SFR_P1, 3);
SBIT(RF_NIRQ,  SFR_P1, 4);
SBIT(RF_PWRDN, SFR_P1, 5);

/* LCD related */
SBIT(LCD_NSEL, SFR_P1, 6);
SBIT(LCD_A0,   SFR_P1, 7);

/* PushButtons */
SBIT(PB1,      SFR_P0, 0);
SBIT(PB2,      SFR_P0, 1);
SBIT(PB3,      SFR_P0, 2);
SBIT(PB4,      SFR_P0, 3);

#define RF_GPIO0  PB1
#define RF_GPIO1  PB2
#define RF_GPIO2  PB3
#define RF_GPIO3  PB4

/*****************************************************************************
 *  Global Enums & Typedefs
 *****************************************************************************/

/*! Enum to declare LED states */
typedef enum
{
  ILLUMINATE,
  EXTINGUISH
} eLEDStates;

/*! SPI device select enum */
typedef enum
{
  RF_NSELECT,
  LCD_NSELECT
} eNSEL;

/*! HW Info struct declaration */
typedef struct
{
  U8* HW_NAME;
  U8* HW_VERSION;
  U8* APP_FW_VERSION;
  U8* APP_FW_NAME;
} tAsciiInfo;

/*****************************************************************************
 *  Global Variable Definitions
 *****************************************************************************/

/*! Global Hardware Info structure */
extern const SEGMENT_VARIABLE(Ascii_Info, tAsciiInfo, SEG_CODE);

/*! LED Blink Control variable */
extern SEGMENT_VARIABLE(LEDBlink,                 U8, SEG_XDATA);

/*! SiLabs Logo */
extern const SEGMENT_VARIABLE(silabs66x30[],      U8, SEG_CODE);

#endif /* HARDWARE_DEFS_H_ */
