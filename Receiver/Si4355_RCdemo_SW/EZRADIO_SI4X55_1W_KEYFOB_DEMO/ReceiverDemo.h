/**
 *  Copyright 2008 Silicon Laboratories, Inc.
 *  http://www.silabs.com
 *
 *  @file ReceiverDemo.h
 *  
 *  C File Description:
 *  @brief TODO
 *
 *  Project Name: dev_EzR2LCD_AESDemo 
 * 
 * 
 *  @author Sz. Papp
 *
 *  @date 2012.04.02.
 *
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 *  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *  This software must be used in accordance with the End User License Agreement.
 */

#ifndef RECEIVERDEMO_H_
#define RECEIVERDEMO_H_

/*****************************************************************************
 *  Global Macros & Definitions
 *****************************************************************************/

/*****************************************************************************
 *  Global Typedefs & Enums
 *****************************************************************************/
/* State machine states */
typedef enum
{
  SM_START,
  SM_STEP1,
  SM_STEP2,
  SM_STEP3,
  SM_STEP4,
  SM_STEP5,
  SM_STEP6,
  SM_STEP7,
  SM_STEP8,
  SM_STEP9,
  SM_STEP10,
  SM_STEP11,
  SM_STEP12,
  SM_STEP13,
  SM_STEP14,
  SM_STEP15
} eSMStates;

typedef struct
{
  eSMStates    ReceiverDemo_State;
  eSMStates    ReceiverDemo_ReturnState;
  volatile U8  ReceiverDemo_BeepLength;
} tReceiverDemo;

typedef struct
{
  U32         ChipID;
  U8          Flags;          /* 4 bit - Buttons */
  U16         RollingCounter;
} tRadioPacket;

/*****************************************************************************
 *  Global Variables
 *****************************************************************************/
extern SEGMENT_VARIABLE(RadioPacket,                tRadioPacket, SEG_XDATA);
extern SEGMENT_VARIABLE(ReceiverDemo_InternalData, tReceiverDemo, SEG_XDATA);

/*****************************************************************************
 *  Global Functions
 *****************************************************************************/
void ReceiverDemo_Init(void);
void ReceiverDemo_Pollhandler(void);


#endif /* AESDEMO_H_ */
