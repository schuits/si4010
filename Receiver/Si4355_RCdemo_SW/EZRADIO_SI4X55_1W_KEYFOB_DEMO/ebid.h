/**
 *  Copyright 2008 Silicon Laboratories, Inc.
 *  http://www.silabs.com
 *
 *  @file ebid.h
 *  
 *  C File Description:
 *  @brief TODO
 *
 *  Project Name: dev_EzR2LCD_AESDemo 
 * 
 * 
 *  @author Sz. Papp
 *
 *  @date 2012.03.30.
 *
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 *  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *  This software must be used in accordance with the End User License Agreement.
 */

#ifndef EBID_H_
#define EBID_H_

/*****************************************************************************
 *  Global Macros & Definitions
 *****************************************************************************/
#define EBID_SMBUS_ADDRESS      0x52

#define EBID_BOARDNAME_LENGTH    21u

/*****************************************************************************
 *  Global Enums & Typedefs
 *****************************************************************************/
typedef struct
{

  U8    EBID_BoardName[EBID_BOARDNAME_LENGTH];
  U16   EBID_Freqband;
  UU16  EBID_EEPromPointer;
} tEBIDInfo;

/*****************************************************************************
 *  Global Variables
 *****************************************************************************/
extern SEGMENT_VARIABLE(EBIDInfo, tEBIDInfo, SEG_XDATA);

/*****************************************************************************
 *  Global Functions
 *****************************************************************************/
U8 EBID_CheckAvailability(void);
U8 EBID_SearchRadioRecord(void);



#endif /* EBID_H_ */
