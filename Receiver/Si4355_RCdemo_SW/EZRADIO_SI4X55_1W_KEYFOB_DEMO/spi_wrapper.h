/**
 *  Copyright 2008 Silicon Laboratories, Inc.
 *  http://www.silabs.com
 *
 *  @file spi_wrapper.h
 *  
 *  C File Description:
 *  @brief TODO
 *
 *  Project Name: EZRadio Si4x55
 * 
 * 
 *  @author Sz. Papp
 *
 *  @date 2012.08.09.
 *
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 *  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *  This software must be used in accordance with the End User License Agreement.
 */

#ifndef SPI_WRAPPER_H_
#define SPI_WRAPPER_H_



/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/

/*! SPI wrapper functions for Si4x55 radio driver */
U8   bSpi_ReadWriteSpi1 (U8);
void vSpi_WriteDataSpi1 (U8, U8 *);
void vSpi_ReadDataSpi1  (U8, U8 *);


#endif /* SPI_WRAPPER_H_ */
