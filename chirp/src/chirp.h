#ifndef _CHIRP_H
#define _CHIRP_H
/*------------------------------------------------------------------------------
 *                          Silicon Laboratories, Inc.
 *                           http://www.silabs.com
 *                               Copyright 2010
 *------------------------------------------------------------------------------
 *
 *    FILE:       tone_demo.h
 *    TARGET:     Si4010
 *    TOOLCHAIN:  Generic
 *    DATE:       May 05, 2010, Wednesday
 *    RELEASE:    2.1 (MPech), ROM version 02.00
 *
 *------------------------------------------------------------------------------
 *
 *    DESCRIPTION:
 *      This file defines a top level header for the tone_demo module.
 *      It should be included in all C files in the tone_demo module.
 *
 *------------------------------------------------------------------------------
 *
 *    INCLUDES:
 */
#include "si4010.h"

// Maximum length of the data buffer to transmit in bytes 
#define bTone_MaxFrameSize_c       (4)
// Requested time interval in between major frequency retuning
// events in 0.256s units. Change this value to set the periodic
// tuning interval. The output PA is turned off during tuning
// for about ~6ms 
#define wTuneInterval_c         (235U)  // 235 .. 60s 

// Constant array which holds the frame
BYTE const code abTone_FrameArray[bTone_MaxFrameSize_c]
        = {0xff, 0xff, 0xff, 0xff};

static tFCast_XoSetup xdata     rXoSetup;

/* Structure for setting up the ODS .. must be in XDATA */
  tOds_Setup xdata rOdsSetup;

/* Structure for setting up the PA .. must be in XDATA */
  tPa_Setup xdata  rPaSetup;

/* Other variables .. can be in either memory. */
  BYTE xdata    *pbFrameHead;
  LWORD         lTime;
  BYTE          bTune;
  float fFrequency;
  LWORD lTimestamp;
  WORD wLEDOnTime;
  WORD wTransmissionTime;
  LWORD lSleepTime;
  WORD wOutputPower;
  WORD wSleepTime;
  WORD wFrequencyShift;
//------------------------------------------------------------------------------

#endif // _CHIRP_H
