/*------------------------------------------------------------------------------
 *                          Silicon Laboratories, Inc.
 *                           http://www.silabs.com
 *                               Copyright 2010
 *------------------------------------------------------------------------------
 *
 *    FILE:       tone_demo_main.c
 *    TARGET:     Si4010
 *    TOOLCHAIN:  Generic
 *    DATE:       May 05, 2010, Wednesday
 *    RELEASE:    2.1 (MPech), ROM version 02.00, trim 3
 *
 *------------------------------------------------------------------------------
 *
 *    DESCRIPTION:
 *      This file contains the main function for the tone demo.
 *      It MUST be run on properly trim part!
 *
 *      Then it tracks frequency with temperature by fine tuning
 *      process by repeatedly calling the vStl_SingleTxLoop() function.
 *      Ultimately, if the temperature is going to drift too much
 *      from what it was during the main tuning vFCast_Tune()
 *      call then the fine tuning process will not be able
 *      to properly track the output frequency with temperature.
 *
 *      For an example how to interrupt to CW tone output after
 *      predefined time, retune by vFCast_Tune(), and start transmitting
 *      tone again see the tone_demo_ptune_main.c for periodic tuning.
 *
 *------------------------------------------------------------------------------
 *
 *    INCLUDES:
 */
#include <stdlib.h>

#include "si4010.h"

#include "si4010_api_rom.h"

#include "chirp.h"


/*------------------------------------------------------------------------------
 *
 *    FUNCTION DESCRIPTION:
 *      Main function for the example.
 *
 *------------------------------------------------------------------------------
 */

void main(void) {

  wOutputPower = 120; //Range = 0 to 127
  wLEDOnTime = 20; //Time led must remain on in Ms
  wTransmissionTime = 20; //Transmittion time in Ms
  
  //for PROD
  lSleepTime = (lSleepTim_GetOneHourValue() / 1800); //sleep time in ticks  ie. 2 seconds
  //for DEBUG
  //at 2.4kHz, 2400 ticks per second, 8640000 ticks per hr therefore 2 seconds = 4800 ticks
  wSleepTime = (lSleepTime/2400)*1000; //sleep time in ms. Change lSleepTime, not this.
  while (1) {
    fFrequency = 151000000; //Units = Hz
	wFrequencyShift = 0; //used for button presses
    //Set DMD interrupt to high priority,
    // any other interrupts have to stay low priority
    PDMD = 1;
    // Disable the Matrix and Roff modes 
    PORT_CTRL &= ~(M_PORT_MATRIX | M_PORT_ROFF | M_PORT_STROBE);
    PORT_CTRL |= M_PORT_STROBE;
    PORT_CTRL &= ~M_PORT_STROBE;
	
	//simple binary combination 2^5 = 32 combinations
	if(!GPIO0)
	{
	wFrequencyShift = wFrequencyShift+1;
	}
	if(!GPIO1)
	{
		wFrequencyShift = wFrequencyShift+2;
	}
	if(!GPIO2)
	{
		wFrequencyShift = wFrequencyShift+4;
	}
	if(!GPIO3)
	{
		wFrequencyShift = wFrequencyShift+8;
	}
	if(!GPIO4)
	{
		wFrequencyShift = wFrequencyShift+16;
	}
	
	if(wFrequencyShift !=0)
	{
		fFrequency = fFrequency + (wFrequencyShift*100000); //eg. Base is 150Mz buttons can shift the frequency from 150.1Mz -> 153.2Mhz (in 0,1Mhz steps)
	}
    // LED control bit off 
    GPIO_LED = 0;

    // Call the system setup. This just for initialization.
    // Argument of 1 just configures the SYS module such that the
    // bandgap can be turned off if needed.  
    vSys_Setup(1);

    // Setup the bandgap for working with the temperature sensor here.
    // bSys_FirstBatteryInsertWaitTime set to non zero value. 
    vSys_BandGapLdo(1);

    // Setup the RTC to tick every 5ms and clear it. Keep it disabled. 
    RTC_CTRL = (0x07 << B_RTC_DIV) | M_RTC_CLR;
    // Enable the RTC 
    RTC_CTRL |= M_RTC_ENA;
    // Enable the RTC interrupt and global interrupt enable 
    ERTC = 1;
    EA = 1;
    //Initialize the master time value
    vSys_SetMasterTime(0);

    // PA setup.  Where PA -. Power amplifier
    // fAlpha and fBeta has to be set based on antenna configuration.
    // Chose a PA level and nominal cap. Both values come from
    // the calculation spreadsheet. 
    rPaSetup.fAlpha = 0.0;
    rPaSetup.fBeta = 0.0;
    //PA transmit power level
    rPaSetup.bLevel = wOutputPower;
    rPaSetup.wNominalCap = 256;
    // Boost bias current to output DAC. Allows for maximum 10.5mA drive. Only LSb bit (bit 0) is used. 
    rPaSetup.bMaxDrv = 1;

    // PA setup.
    vPa_Setup( & rPaSetup);

    // ODS setup .. results from calculation spreadsheet. Where ODS -> Output data serializer
    // Set to 1kbps OOK 
    rOdsSetup.bModulationType = 0; // 1 .. FSK, 0 .. OOK 
    rOdsSetup.bClkDiv = 5;
    rOdsSetup.bEdgeRate = 0;

    // Set group width to 7, which means 8 bits/encoded byte to be transmitted.
    // The value must match the output width of the data encoding function
    // set by the vStl_EncodeSetup() below. 
    rOdsSetup.bGroupWidth = 7;
    rOdsSetup.wBitRate = 4000; // 1kbps 

    // Configure the warm up intervals LC: 8, DIV: 5, PA: 4 
    rOdsSetup.bLcWarmInt = 8;
    rOdsSetup.bDivWarmInt = 5;
    rOdsSetup.bPaWarmInt = 4;

    // ODS setup 
    vOds_Setup( & rOdsSetup);

    // Setup the STL encoding for none. No encode function therefore we
    // leave the pointer at NULL. 
    vStl_EncodeSetup(bEnc_NoneNrz_c, NULL);

    // Generate the frame to use. Example array is fixed in CODE
    // as constant for this example. Assign the beginning of the array
    // to the pbFrameHead pointer. 
    pbFrameHead = (BYTE xdata * ) abTone_FrameArray;

    // Setup frequency casting .. needed to be called once per boot 
    vFCast_Setup();

    // Run at preSet MHz.  Continue to tune frequency between frames.
    // This is not absolutely necessary as the Single transmission loop
    // also adjusts for temperature drift. It calculate values used by
    // the vStl_SingleTxLoop() during transmission. Input is in [Hz]. 
    vFCast_Tune(fFrequency);

    // Now wait until there is a demodulated temperature sample. Where DMD ISR is Temperature Sensor Interrupt Service
    while (0 == bDmdTs_GetSamplesTaken()) {}

    // Tune the PA with the current temperature as an argument 
    vPa_Tune(iDmdTs_GetLatestTemp());

    vSys_LedIntensity(3); //0.97 mA

    // Encoding is none .. 0. Force first byte to turn the PA on.
    // Set the all 1's output if there is no data .. value 3 below. 
    ODS_CTRL = (ODS_CTRL & (~M_ODS_SHIFT_CTRL)) | (3 << B_ODS_SHIFT_CTRL);

    // get current timestamp  
    //lTimestamp = lSys_GetMasterTime();

    //GPIO_LED = 1; //Turn LED on
    //while ((lSys_GetMasterTime() - lTimestamp) < wLEDOnTime) //ms
    //{
      //wait for LED ON time to expire
    //}
    //GPIO_LED = 0; //turn LED off

    //-- Run the Single Tx Loop 
    vStl_PreLoop();

    lTimestamp = lSys_GetMasterTime();
	GPIO_LED = 1;
    while ((lSys_GetMasterTime() - lTimestamp) < wTransmissionTime) {
      // Wait repeat interval. 
      //Transmit packet
      vStl_SingleTxLoop(pbFrameHead, 1);
    }
    vStl_PostLoop();
	GPIO_LED = 0;

    //if part is burned to user or run mode.
    if ((PROT0_CTRL & M_NVM_BLOWN) > 1) {
      // enable Sleep timer
      vSleepTim_SetCount(lSleepTime | 0x01000000);

      //Disable all interrupts
      EA = 0;
      // Shutdown
      vSys_Shutdown();
    } else {
      lTimestamp = lSys_GetMasterTime();
      while ((lSys_GetMasterTime() - lTimestamp) < wSleepTime) //ms
      {
        //mock a sleep
      }
    }
  }

  // This will never happen. 
  return;
}

//------------------------------------------------------------------------------